package com.testtaskandroid.views;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.testtaskandroid.R;
import com.testtaskandroid.models.EnergyStorage;
import com.testtaskandroid.presenters.MainActivityPresenter;
import com.testtaskandroid.utils.ShakeListener;

import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity implements MainActivityPresenter.MainActivityViewProtocol{

    MainActivityPresenter presenter;
    private ShakeListener mShaker;
    TextView message;
    Button statistick;
    LinearLayout message_container;
    Timer dismis_timer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        presenter = new MainActivityPresenter(this);
        presenter.attachStorage(new EnergyStorage(this));
        findIds();
        mShaker = new ShakeListener(this);
        mShaker.setOnShakeListener(new ShakeListener.OnShakeListener () {
            public void onShake()
            {
                presenter.shake();
            }
        });
        mShaker.resume();
        showNotification("Пожалуйста потратьте энергию",5);
    }



    private void findIds(){ // Find all elements and listeners

        message = (TextView)findViewById(R.id.message);
        message_container = (LinearLayout)findViewById(R.id.message_container);
        statistick = (Button)findViewById(R.id.statistickButton);
        statistick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this,HistoryActivity.class));
            }
        });
    }

    private boolean isNotificationVisible(){
        return  message_container.getAlpha() == 1f;
    } // Check if currently notification open


    /**
     * show notification
     *  @param text text of notification
     *  @param seconds second how long it should appear
     */
    @Override
    public void showNotification(final String text,final int seconds) { // show notification and dismis it on delay
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (!isNotificationVisible() || text == "Энергия успешно потрачена"){
                    message.setText(text);
                    message_container.animate().alpha(1f);
                    if (dismis_timer!=null){
                        dismis_timer.cancel();
                        dismis_timer = null;
                    }
                    dismis_timer = new Timer();
                    dismis_timer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            dismisNotifications();
                        }
                    },seconds*1000);
                }

            }
        });
    }

    private void dismisNotifications() {
        dismis_timer = null;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (isNotificationVisible()){
                    message_container.animate().alpha(0);
                }
            }
        });
    } // dissmiss notification

    @Override
    protected void onResume() {
        super.onResume();
        mShaker.resume();
        presenter.startTimer();
    }


    @Override
    protected void onPause() {
        super.onPause();
        mShaker.pause();
        presenter.pauseTimer();
    }
}
