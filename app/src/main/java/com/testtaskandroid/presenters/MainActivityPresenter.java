package com.testtaskandroid.presenters;

import com.testtaskandroid.models.Energy;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Morgot on 10/10/2018.
 */

public class MainActivityPresenter {

    /**
     *  @param timer  timer that work whole time on activity
     */
    Timer allTimeTimer;

    /**
     *  @param seconds_counter seconds while phone is shaking
     */
    int seconds_counter = 0;

    /**
     *  @param reset_counter count of resets
     */
    int reset_counter = 0;

    /**
     *  @param message_counter counter for message of none activity showing
     */
    int messageCounter = 0;

    /**
     *  @param message_counter counter for message of none activity showing
     */
    Date lastShake = null;

    /**
     *  @param allTimer task task for our timer
     */
    AllTimeTimerTask allTimeTimerTask;

    /**
     * Main Activity protocol
     */
    public interface MainActivityViewProtocol{

        void showNotification(String text,int seconds);
    }

    /**
     * Energy storage protocol
     */
    public interface EnergyStorageProtocol{ // Energy sotrage protocol

        void writeToStorage(Energy energy);
    }



    private  MainActivityViewProtocol view;
    private EnergyStorageProtocol storage;

    /**
     * public constructor
     */
    public MainActivityPresenter(MainActivityViewProtocol view){
        this.view = view;
    }

    /**
     *  atach storage to protocol
     */
    public void attachStorage(EnergyStorageProtocol storage){
        this.storage = storage;
    }

    /**
     *  event shacke listener
     */
    public void shake(){
        view.showNotification("Вы тратите энергию",3);
        lastShake = new Date();
    }

    /**
     *  timer task
     */
    class AllTimeTimerTask extends TimerTask {


        /**
         *  on each seconds we check few things:
         *  1) if phone was sheked
         *  2) time between last shakes
         *  3) seconds while phone is shacked
         *
         *
         *  If phone was shaked we will increese counter of seconds
         *  If Counter is 10 we will send message to storage that we have new event
         *  Nad push show notification on view
         *
         *  If difference between counter is more than one we make seconds timer0, and increase reset counter
         *  If reset counter is 3 wi will update storage with info that we have new try ebent and will show notification on view
         *
         *
         *  If time of none activity is 10 we will show notification
         *
         *
         */
        @Override
        public void run() {

            Date now = new Date();

            if(lastShake!=null){

                int differnce = (int)((now.getTime() - lastShake.getTime())/1000);

                if(differnce<2){
                    seconds_counter ++;
                    if(seconds_counter == 10){
                        seconds_counter = 0;
                        reset_counter = 0;
                        view.showNotification("Энергия успешно потрачена",3);
                        createEvent(Energy.EnergyStatus.spended);

                    }
                }else{
                    seconds_counter = 0;
                    reset_counter ++;
                    lastShake = null;
                    if(reset_counter == 3){
                        createEvent(Energy.EnergyStatus.start_spending);
                        reset_counter = 0;
                    }
                }
            }else{

                messageCounter ++;
                if(messageCounter == 5){

                    view.showNotification("Пожалуйста потратьте энергию",5);
                    messageCounter = 0;
                }

            }

        }
    }

    /**
     *  pause timer on activity life cycle
     */
    public void pauseTimer(){
        if(allTimeTimer!=null) {
            allTimeTimer.cancel();
        }
        allTimeTimerTask = null;
        allTimeTimer = null;
        lastShake = null;
        seconds_counter = 0;
    }

    /**
     *  start timer on activity life cycle
     */
    public void startTimer(){
        allTimeTimer = new Timer();
        allTimeTimerTask = new AllTimeTimerTask();
        allTimeTimer.scheduleAtFixedRate(allTimeTimerTask,0,1000);
    }

    /**
     *  create event and push it to storage
     */
    private void createEvent(Energy.EnergyStatus status){

        Energy energy = new Energy(status);
        storage.writeToStorage(energy);
    }





}
