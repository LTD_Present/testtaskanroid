package com.testtaskandroid.models;

import android.content.Context;

import com.testtaskandroid.presenters.MainActivityPresenter;
import com.testtaskandroid.utils.Logger;
import com.testtaskandroid.utils.URLRequest;

/**
 * Created by Morgot on 10/10/2018.
 */

public class EnergyStorage implements MainActivityPresenter.EnergyStorageProtocol, URLRequest.URLRequestDelegate {


    HistoryManager historyManager; // Manager to work with files

    public EnergyStorage(Context context){
        historyManager = new HistoryManager(context);
    }

    /**
     *  write data to file and web
     *  @param energy object that need to be saed
     */
    @Override
    public void writeToStorage(Energy energy) { // Write to storage and send to web
        historyManager.write(energy);
        new URLRequest(energy,this);
    }

    @Override
    public void URLRequestSuccessForUrlandResponseData(String url, String data) {// listeners from web

        Logger.print(data);
    }

    @Override
    public void URLRequestFailForUrlwithError(int responseCode, Exception exception) { // listeners from web
        Logger.print(exception.getMessage());
    }

    @Override
    public void URLRequestProgressForUrlwithError(String url, int value) {// listeners from web
        Logger.print("Response is "+ value);
    }
}
