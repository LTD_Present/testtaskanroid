package com.testtaskandroid.models;

/**
 * Created by Morgot on 10/10/2018.
 */

public class Energy {

    /**
     *  Enum for storing energy status
     */
    public enum EnergyStatus{ // enum for status of energy using
        spended,start_spending;

        public static EnergyStatus fromInteger(int value) {
            switch(value) {
                case 0:
                    return spended;
                case 1:
                    return start_spending;
            }
            return null;
        }
    }

    /**
     *  @param status status of energy
     */
    private EnergyStatus status;

    /**
     *  Constructor
     *  @param status status of new energy
     */
    public Energy(EnergyStatus status){ // construct with default status
        this.status = status;
    }


    /**
     *  Presentation of object in web
     *
     */
    public String webPresentation(){ //presentation fro network manager
        return String.format("{\"status\": %d}", status.ordinal());
    }

    /**
     *  Presentation of object to file
     */
    public String filePresentation(){ // presentation for File
        return String.format("%d,",status.ordinal());
    }

    /**
     *  To look more preety :)
     */
    @Override
    public String toString() {
        return  webPresentation();
    }
}
