package com.testtaskandroid.models;

import android.content.Context;

import com.testtaskandroid.utils.Logger;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

/**
 * Created by Morgot on 10/10/2018.
 */

public class HistoryManager {

    private Context context; // context to work with file
    private String FILE_NAME = "data.txt"; // file name

    private HistoryManager(){} // close constructor

    /**
     * Constructor
     *  @param context context to work with files
     */
    public HistoryManager(Context context){
        this.context = context;
    }


    /**
     * write data to file
     *  @param energy object to save
     */
    public void write(Energy energy) { //appen d to file new data

        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput(FILE_NAME,Context.MODE_APPEND));
            outputStreamWriter.append(energy.filePresentation());
            outputStreamWriter.close();
        }
        catch (IOException e) {

            Logger.print((e.getMessage()));
        }
    }

    /**
     *  Read file and parse data
     */
    public ArrayList<String> read(){ // read data from file and prepare statisticks

        String result = "";
        int positive_counter = 0;
        int unpositive_counter = 0;
        try {
            InputStream inputStream = context.openFileInput(FILE_NAME);

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ( (receiveString = bufferedReader.readLine()) != null ) {
                    stringBuilder.append(receiveString);
                }

                inputStream.close();
                result = stringBuilder.toString();
            }
        }
        catch (FileNotFoundException e) {
            Logger.print(e.toString());
        } catch (IOException e) {
            Logger.print(e.toString());
        }

       String[] not_parsed_data = result.split(",");
       for (String energy_string : not_parsed_data){
           if(energy_string.isEmpty()){
               break;
           }
           int energy_status = Integer.parseInt(energy_string);
           if(energy_status == 0){
               positive_counter ++;
           }else{
                unpositive_counter ++;
           }
       }
       ArrayList<String>res = new ArrayList<>();
       res.add(String.format("Енергии потрачено: %d раз",positive_counter));
       res.add(String.format("Попыток траты енергии: %d раз",unpositive_counter));
       return res;
    }



}
