package com.testtaskandroid.utils;

import android.os.AsyncTask;

import com.testtaskandroid.models.Energy;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by test on 28.05.15.
 */
public class URLRequest extends AsyncTask{

    private int responseCode;

    public interface URLRequestDelegate {
        void URLRequestSuccessForUrlandResponseData (String url, String data);
        void URLRequestFailForUrlwithError(int responseCode, Exception exception);
        void URLRequestProgressForUrlwithError (String url, int value);
    }

    private String mURLString, mResponseData = null ,mMethod = null;
    private URLRequestDelegate mDelegate;
    private String mPostDataString = null;
    private Exception exception = null;



    public URLRequest(Energy energy, URLRequestDelegate delegate) {
        mURLString = "https://betaapi.nasladdin.club/api/energy/operationStatus";;
        mPostDataString = energy.webPresentation();
        mDelegate = delegate;
        mMethod = "PUT";
        execute();
    }





    @Override
    protected Object doInBackground(Object[] params) {

        URL url;
        try {
            url = new URL(mURLString);
        String response = "";
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setDoInput(true);
        connection.setReadTimeout(60000);
        connection.setConnectTimeout(60000);


        if (mPostDataString!=null) {
            connection.setRequestMethod(mMethod);
            if (mPostDataString != null && !mPostDataString.isEmpty()) {
                connection.setRequestProperty("Content-Type", "application/json");
            }
            connection.setDoOutput(true);
            OutputStream os = connection.getOutputStream();
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
            bw.write(mPostDataString);
            bw.flush();
            bw.close();
            os.close();
        }
        else {
            try {
                connection.setRequestMethod("GET");
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
        }

            Logger.print("Logout: requestUrl: " + mURLString + ", mPostDataString: " + mPostDataString);
            responseCode = connection.getResponseCode();
            Logger.print("Logout: requestUrl: " + mURLString + ", responseCode: " + responseCode);
            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                int size = 600000;
                BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                while ((line=br.readLine()) != null) {
                    response+=line;
                    mDelegate.URLRequestProgressForUrlwithError(mURLString, response.length()/size);
                }
                br.close();
                Logger.print("Logout: requestUrl: " + mURLString + ", responseIf200: " + response);
                mResponseData = response;
            } else {
                String APIError;
                BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                while ((APIError=br.readLine()) != null) {
                    response+=APIError;
                }
                br.close();
                Logger.print("Logout: requestUrl: " + mURLString + ", responseIfNOT200: " + response);
                mResponseData = null;
                exception = new Exception("URLRequestEmptyResponse");
            }

            if (mResponseData != null && mResponseData.isEmpty()) {
                exception = new Exception("URLRequestEmptyResponse");
            }
        }
        catch (IOException e) {
            Logger.print("Logout: requestUrl: " + mURLString + ", exception: " + e);
            exception = e;
        }

        return null;
    }

    @Override
    protected void onPostExecute(Object o) {
        if (!isCancelled()) {
            if (exception != null) {
                mDelegate.URLRequestFailForUrlwithError(responseCode, exception);
            } else if (mResponseData != null) {
                mDelegate.URLRequestSuccessForUrlandResponseData(mURLString, mResponseData);
            }
        }
    }
}